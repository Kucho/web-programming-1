;(function ($) {
  'use strict'

  /* --------------------------
   brands-carousel
   ---------------------------- */
  $('.brands-carousel').slick({
    dots: false,
    arrows: false,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    adaptiveHeight: true,
    prevArrow: '<i class="fa fa-angle-left"></i>',
    nextArrow: '<i class="fa fa-angle-right"></i>',
    responsive: [
      {
        breakpoint: 992,
        settings: {
          arrows: false,
          slidesToShow: 3
        }
      },
      {
        breakpoint: 361,
        settings: {
          arrows: false,
          slidesToShow: 2
        }
      }
    ]
  })
})(jQuery)
